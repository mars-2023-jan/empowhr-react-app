import React from 'react';
import { useContext } from 'react';
import { Link } from 'react-router-dom';
import MyContext from './MyContext';
import empowhr from './assets/empowhr-logo-01.png';

function Main({currentUser, authUser}) {
    //const currentUser = useContext(MyContext);
    console.log(currentUser)
    return (
        <div>
            <center>
                <h1>EmpowHR Main</h1>
                {currentUser ? <div>User <strong>{currentUser}</strong></div>: <div>No User</div>}
            </center>
            <br />
        </div>
    );
}

export default Main;
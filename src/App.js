import { useState } from 'react';
import { useNavigate, BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { Navbar, Nav, NavItem, NavLink } from "react-bootstrap";
import Main from './Main';
//import Dashboard from './Dashboard';
import Login from './components/login-components/Login';
import LoginSuccess from './components/login-components/LoginSuccess';
import LoginFail from './components/login-components/LoginFail';
import LoginForgot from './components/login-components/LoginForgot';
import Onboarding from './components/onboarding/Onboarding';
import Roster from './components/roster/Roster';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import './components/login-components/Login.css';
import empowhr from './assets/empowhr-logo-01.png';


const App = () => {
  const [authUser, setAuthUser] = useState("")
  const [currentUser, setCurrentUser] = useState("")
  const [showLogin, setShowLogin] = useState(true)
  const handleShowLogin = () => setShowLogin(true)

  const [activeRoute, setActiveRoute] = useState('main');
  const handleRouteChange = (route) => {
    setActiveRoute(route);
  };
  let currentComponent;
  switch (activeRoute) {
    case 'main':
      currentComponent = <Main currentUser={currentUser} />;
      break;
    case 'roster':
      currentComponent = <Roster currentUser={currentUser} />;
      break;
    case 'onboarding':
      currentComponent = <Onboarding currentUser={currentUser} />;
      break;
    default:
      currentComponent = <Main currentUser={currentUser} />;
  }

  return (
    <>
        <Navbar bg="light" variant="light" style={{'border-bottom':'2px solid black'}}>
          <Navbar.Brand href="#">
            <img src={empowhr} alt="Logo" height="30px" />
          </Navbar.Brand>
          <Nav>
            <NavItem>
              <NavLink href="#" onClick={() => handleRouteChange('main')}>Home</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#" onClick={() => handleRouteChange('roster')}>Roster</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#" onClick={() => handleRouteChange('onboarding')}>Onboarding</NavLink>
            </NavItem>
            <NavItem>
              
              {authUser ?
                <span style={{ float: 'right', border:'1px solid #888', borderRadius:'.5em', padding:'2px' }}>
                  {authUser}
                  <Button onClick={() => { setAuthUser(""); setCurrentUser("") }}>Logout</Button> 
                </span>
              :  
                <Button onClick={() => handleShowLogin()}>Login</Button>
              } 

            </NavItem>
          </Nav>
        </Navbar>
      
        
     
      {currentComponent}

      <Login showLogin={showLogin} setShowLogin={setShowLogin} currentUser={currentUser} setCurrentUser={setCurrentUser} authUser={authUser} setAuthUser={setAuthUser} />
    </>
  )

};

export default App;
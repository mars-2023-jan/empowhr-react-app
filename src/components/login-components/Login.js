import React, { useState, useContext } from "react";
import { Modal, Button, Form, Row, Col } from "react-bootstrap";
import { InputGroup } from "react-bootstrap";
import { BsFillPersonFill } from "react-icons/bs";
import { MdOutlineVisibility } from "react-icons/md";
import { MdOutlineVisibilityOff } from "react-icons/md";
import empowhr from "../../assets/empowhr-logo-01.png";
import mars1 from "../../assets/mars1.png";
import { isVisible } from "@testing-library/user-event/dist/utils";
import axios from "axios";




const Login = ({showLogin, setShowLogin, currentUser, setCurrentUser, authUser, setAuthUser}) => {
  // const authUser = useContext(MyContext);
  // const [currentUser, setCurrentUser] = useState("")
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [styleEmail, setStyleEmail] = useState({});
  const [stylePassword, setStylePassword] = useState({});
  const [showPassword, setShowPassword] = useState(false);
  const [emailValid, setEmailValid] = useState(false);
  const [passwordValid, setPasswordValid] = useState(false);
  const [isDisabled, setIsDisabled] = useState(true);
  const [passDisabled, setPassDisabled] = useState(true);
  const [eMsgVisible, setEMsgVisible] = useState(false);
  const [pMsgVisible, setPMsgVisible] = useState(false);
  const [showFailedLogin, setShowFailedLogin] = useState(false);
  const handleSetAuthUser = (user) => {
    setCurrentUser(user);
    setAuthUser(user);
  };
  //handleSetAuthUser("rick@rick.com");
  const handleClickEye = () => { setShowPassword(!showPassword); };
  const handleInputEmail = (event) => {
    let email = event.target.value;
    setEmail(event.target.value);
    setCurrentUser(email);
    var re = new RegExp(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z]{2,6}$/);

    if (re.test(email)) {
      setStyleEmail({ border: "1px solid #D5DBDB" });
      //console.log("Valid Email:" + this.currentUser);
      setEmailValid(true);
      setEMsgVisible(false);
      console.log(eMsgVisible);
      //setIsDisabled(true);
      // if (passwordValid) {
      //   setIsDisabled(false);
      // }
    } else {
      if (email !== "") {
        setCurrentUser(email ? email : "No User")
      }
      setStyleEmail({
        border: "1px solid red",
        boxShadow: "0px 0px 10px #F1948A ",
      });
      setEmailValid(false);
      setEMsgVisible(true);
      console.log(eMsgVisible);
      // setIsDisabled(true);
    }
  };

  const handleInputPassword = (event) => {
    let password = event.target.value;
    setPassword(password);
    //console.log(password);
    var re2 = new RegExp(
      /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/
    );
    // remove the "true ||" to make the password validation work
    if (true || re2.test(password)) {
      console.log("Valid Password");
      setStylePassword({ border: "1px solid #D5DBDB" });
      setPasswordValid(true);
      setPMsgVisible(false);
      console.log(eMsgVisible);
      setPasswordValid(true);
      setIsDisabled(false);
      // if (emailValid) {
      //   setIsDisabled(false);
      // }
    } else {
      //console.log("Invalid Password");
      setStylePassword({
        border: "1px solid red",
        boxShadow: "0px 0px 10px #F1948A ",
      });
      setPasswordValid(false);
      setPMsgVisible(true);
      console.log(pMsgVisible);
      setIsDisabled(true);
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault()
    console.log("")
    axios.defaults.baseURL = 'http://localhost:5000';
    axios.post(`/api/user/login`, {
      email: email,
      password: password
    })
      .then(res => {
        if (res.data) {
          // that email/password combo exists, authenticate user
          // set a state variable for currentUser to the user object
          setShowFailedLogin(false)
          //navigate("/login_success", { state: { username: res.data } })
          // want to handleSetAuthUser on the entire user object
          console.log("res.data: " + res.data)
          console.log(res.data.name)

          handleSetAuthUser(res.data)

          handleCloseLogin()
        } else {
          // that email/password combo doesn't exist, reject user
          setShowFailedLogin(true)
        }
      })
      .catch(err => {
        console.log("Error: " + err)
        setShowFailedLogin(true)
      })

    return;
  }

  const handleEmailBlur = (event) => {
    axios.defaults.baseURL = "http://localhost:5000";
    axios
      .get(`/api/user/${email}`)
      .then((res) => {
        if (res.data) {
          console.log("On blur");
          setPassDisabled(false);
          setFocusOnPassword();
        } else {
          console.log("doesn't exist");
          setPassDisabled(true);
          setStyleEmail({
            border: "1px solid red",
            boxShadow: "0px 0px 10px #F1948A ",
          });
        }
      })
      .catch((err) => {
        console.log("Error" + err);
        setPassDisabled(true);
        setStyleEmail({
          border: "1px solid red",
          boxShadow: "0px 0px 10px #F1948A ",
        });
      });

    return;
  };
  const setFocusOnPassword = () => {
    const passwordInput = document.getElementById("formPlaintextPassword");
    passwordInput.focus();
  };
  
  const handleCloseLogin = () => setShowLogin(false);
  const handleShowLogin = () => setShowLogin(true);
  const enabled = emailValid && passwordValid;
  return (
    <div>
      <Modal show={showLogin} onHide={handleCloseLogin}>
        <Modal.Header>
          <Modal.Title>
            <img src={mars1} alt="empowhr" width="100px;" />
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p style={{ textAlign: "center", marginTop: "1em" }}>
            <img src={empowhr} alt="mars" className="mars-1" width="360px;" />
          </p>
          <p
            id="failed_login"
            style={{
              color: "#aa2222",
              fontStyle: "bold",
              fontSize: "1.25em",
              marginBottom: "0em",
              display: showFailedLogin ? "block" : "none",
            }}
          >
            Login Failed. Please try again.
          </p>
          {/* <Form onSubmit={handleSubmit}> */}
          <Form onSubmit={handleSubmit}>
            <Form.Group as={Row} className="mb-3" controlId="formUsername">
              <Form.Label column sm="2" visuallyHidden>
                Username
              </Form.Label>
              <InputGroup className="mb-2">
                <Form.Control
                  type="email"
                  value={email}
                  placeholder="Enter email address"
                  style={styleEmail}
                  className="myInput"
                  onChange={handleInputEmail}
                  onBlur={handleEmailBlur}
                />
                <InputGroup.Text style={{ fontSize: "larger" }}>
                  <BsFillPersonFill />
                </InputGroup.Text>
              </InputGroup>
              {eMsgVisible && (
                <div className="error">Please enter a valid email</div>
              )}
            </Form.Group>
            <br />
            <Form.Group
              as={Row}
              className="mb-3"
              controlId="formPlaintextPassword"
            >
              <Form.Label column sm="2" visuallyHidden>
                Password
              </Form.Label>
              <InputGroup className="mb-2">
                <Form.Control
                  type={showPassword ? "text" : "password"}
                  value={password}
                  placeholder="Enter password"
                  style={stylePassword}
                  className="myInput"
                  onChange={handleInputPassword}
                  disabled={passDisabled}
                />
                <InputGroup.Text
                  style={{ fontSize: "larger" }}
                  onClick={handleClickEye}
                >
                  {/* TODO: toggle password visibility is not working */}
                  {showPassword ? (
                    <MdOutlineVisibility onClick={handleClickEye} />
                  ) : (
                    <MdOutlineVisibilityOff onClick={handleClickEye} />
                  )}
                </InputGroup.Text>
              </InputGroup>
              {pMsgVisible && (
                <div className="error">Please enter a valid password</div>
              )}
            </Form.Group>
            <br />
            <p style={{ textAlign: "right", fontSize: "small" }}>
              <a href="#">Forgot password?</a>
            </p>
            <br />
            <button disabled={isDisabled} type="submit" id="btn-login">
              LOGIN
            </button>
          </Form>
          <p></p>
        </Modal.Body>

        <Modal.Footer>
          <Modal.Title>&copy; MARS Solutions Group</Modal.Title>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default Login;

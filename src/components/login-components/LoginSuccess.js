import React from 'react';
import { useLocation } from "react-router-dom";


const LoginSuccess = ({currentUser}) =>  {
    //const [currentUser, setCurrentUser] = useState("");
    const { state } = useLocation();
    const {username} = state;
    return (
        <div>
            <center><h2>Login Successful.  Welcome, {username}!</h2></center>
        </div>
    );
}

export default LoginSuccess;
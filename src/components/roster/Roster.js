import React from 'react';

function Roster({currentUser}) {
    return (
        <div>
            <h1>Employee Roster</h1>
            The current user is: {currentUser}
        </div>
    );
}

export default Roster;
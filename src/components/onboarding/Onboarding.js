import React from 'react';
import { useState, useEffect } from 'react';
import axios from 'axios';
import Tasks from './Tasks';

const Onboarding = ({currentUser}) => {
  return ("<h1>Onboarding</h1>");
    let [loading, setLoading] = useState(true);
    let [allTasks, setAllTasks] = useState([]);
  
    const fetchTasks = async () => {
      try {
        const res = await axios.get(`/api/tasks/${currentUser}}`);
        if (res.data) {
          console.log(res.data)
          setAllTasks(res.data);
        }
      } catch (err) {
        console.log(err);
      }
    };
  
    useEffect(() => {
      fetchTasks();
      setLoading(false);
    }, []);
  
    return (
      <div>
        <h1>Onboarding</h1>
        The current user is: {currentUser}
        {/* {loading ? <p>Loading...</p> : typeof(allTasks)} */}
        {loading ? <p>Loading...</p> :  <Tasks allTasks={allTasks}/>}
      </div>
    );
  };
  
  export default Onboarding;
  